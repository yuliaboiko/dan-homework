'use strict'

document.addEventListener('DOMContentLoaded', () => {

   let input = document.querySelector('#price');
   let div = document.querySelector('div');
   let p = document.querySelector('p');

   //Создание спана
   let span = document.createElement('span');
   span.classList.add('tag');

   input.addEventListener('focus', function () {
      this.style.outlineColor = 'green';
      reset(this);

   })

   input.addEventListener('blur', function () {

      if (checkValue(this)) {
         span.innerHTML = `Текущая цена: ${this.value}`;
         div.append(span);
         createCloseBtn(span);
         this.style.color = 'green';
      } else {
         span.remove();
         createErrorText('Please enter correct price');
      };

   })

   //Обнуление для фокуса
   function reset(input) {
      input.style.color = '';
      input.value = '';
      input.style.borderColor = '';
      p.innerHTML = '';
   }

   //Создание кнопки для удаление метки(спана)
   function createCloseBtn(parent) {
      let btn = document.createElement('button');
      btn.innerHTML = '&#x2715;';
      btn.classList.add('delete-btn');
      removeSpan(btn);
      parent.append(btn);
      return btn;
   }

   //Удаление метки (спана)
   function removeSpan(elem) {
      elem.addEventListener('click', function () {
         elem.parentNode.remove();
         input.value = '';
      })
   }

   //проверка данных с инпута на валидность
   function checkValue(input) {
      if (input.value < 0 || input.value === '') {
         input.style.borderColor = 'red';
         return false;
      } else {
         return true;
      }
   }

   //вывод ошибки при невалидности данных в инпуте
   function createErrorText(text) {
      p.innerHTML = text;
   }

});
