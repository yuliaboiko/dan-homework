'use strict'

let tabsText = document.querySelector('.tabs-content');
let tabs = document.querySelector('.tabs');

tabs.addEventListener('click', function (event) {

   if (event.target.tagName === 'LI') {
      deleteActive(tabs);
      addActive(event.target);
      findDataText(
         event.target.dataset.title
      );
   }

})

//Выбор подходящего текста
function findDataText(dataTitle) {
   deleteActive(tabsText);
   let currentText = tabsText.querySelector(`li[data-text^=${dataTitle}]`);
   addActive(currentText);
}

// Удаление класса
function deleteActive(parent) {
   let active = parent.querySelector('.active');
   active.classList.remove('active');
}

// Добавление класса
function addActive(elem) {
   elem.classList.add('active');
}