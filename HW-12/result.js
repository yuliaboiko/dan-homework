'use strict'

const images = document.querySelectorAll('.image-to-show');
const imgBox = document.querySelector('.images-wrapper');
let timerId = setInterval(changeActiveSlide, 3000);

function changeActiveSlide() {

   let active = document.querySelector('.active');
   let activeNum = active.dataset.num;
   //Удаляем класс актив у текущего слайда
   active.classList.remove('active');
   //Присваиваем класс актив следующему слайду
   if (activeNum < images.length) {
      images[activeNum++].classList.add('active');
   } else {
      //Если все слайды закончились - присвоить 1 слайду класс актив
      images[0].classList.add('active');
   }

}

//создание кнопок
function createBtn(text, btnClass, func) {
   let btn = document.createElement('button');
   btn.innerHTML = text;
   btn.classList.add(btnClass);
   btn.addEventListener('click', func);
   imgBox.after(btn);
}

createBtn('Возобновить показ', 'play', playSlide);
createBtn('Прекратить', 'stop', stopSlide);

let playBtn = document.querySelector('.play');
let stopBtn = document.querySelector('.stop');

playBtn.removeEventListener('click', playSlide);

//Остановить слайдер
function stopSlide() {
   clearInterval(timerId);
   playBtn.addEventListener('click', playSlide);
}

//Возобновить слайдер
function playSlide() {
   timerId = setInterval(changeActiveSlide, 3000);
   this.removeEventListener('click', playSlide);
}

