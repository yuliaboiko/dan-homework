'use strict'
let salaries = {
   development: {
      "John": 500,
      "Pete": 300,
   },
   sales: {
      "Mary": 500,
      "Kate": 300,
   }
};



function cloneObj(obj) {
   let newObj;
   if (Array.isArray(obj)) {
      newObj = [];
      newObj = obj.map(elem => elem = cloneObj(elem));
   } else {
      newObj = {};
      for (let [key, value] of Object.entries(obj)) {
         if (typeof value !== 'object') {
            newObj[key] = value;
         } else if (typeof value === 'object') {
            newObj[key] = cloneObj(obj[key]);
         }
      }
   }
   return newObj;
}

//Новый объект
let resultObj = cloneObj(salaries);

console.log(resultObj);
console.log(salaries);

//Меняем объект salaries
salaries.development.John = 600;
salaries.development.Jane = 1000;

// Изменения salaries на resultObj не влияют
console.log(salaries);
console.log(resultObj);


// Внешние объекты не равны
console.log(salaries === resultObj);

// Внутренние объекты не равны
console.log(salaries.development === resultObj.development);



