'use strict'

document.addEventListener("DOMContentLoaded", function () {
   let body = document.body;
   let btn = document.querySelector('.theme-btn');

   setDefaultTheme();

   // Добавление/удаление класса темной темы в css
   function setDefaultTheme() {
      if (localStorage.getItem('theme') === 'dark') {
         body.classList.add('dark');
      } else {
         body.classList.remove('dark')
      }
   }

   // чтение/запись в local storage
   btn.addEventListener('click', function () {
      if (localStorage.getItem('theme')) {
         localStorage.removeItem('theme');
      } else {
         localStorage.theme = 'dark';
      }

      setDefaultTheme()
   })

})

