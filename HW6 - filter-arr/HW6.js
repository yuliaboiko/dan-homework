'use strict'

let filterBy = (arr, typeOfVal) => {
   let filteredArr = arr.filter(elem => typeof elem !== typeOfVal);
   return filteredArr;
}

let arr = [1, 2, 'hello', true, {}, 'world', null, undefined];
console.log(filterBy(arr, 'number'));