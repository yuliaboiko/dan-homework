'use strict'

const student = {
   table: {},
   //Установка данных студента
   setUserData() {
      this.name = prompt('Введите Ваше имя');
      this.lastName = prompt('Введите Вашу фамилию');
   },
   //Формирует объект с оценками table
   getTableInfo() {
      while (true) {
         let subject = prompt('Введите название предмета');

         if (subject == null || subject == '') {
            break;
         }
         let mark = prompt('Введите оценку по этому предмету');
         this.table[subject] = mark;
      }
   },
   //Считает плохие оценки
   countBadMarks() {
      let counter = 0;
      for (let key in this.table) {
         if (+this.table[key] < 4) {
            counter++;
         }
      }
      if (!counter) {
         alert('Студент переведен на следующий курс');
      }
   },
   //Считает средний балл
   countAverageMark() {
      let sumOfMarks = 0;
      let count = 0;
      for (let key in this.table) {
         sumOfMarks += +this.table[key];
         count++;
      }
      if ((sumOfMarks / count) > 7) {
         alert('Студенту назначена стипендия');
      }
   }

};

student.setUserData();
student.getTableInfo();
student.countBadMarks();
student.countAverageMark();

console.log(student);