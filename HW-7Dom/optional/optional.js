'use strict'

let body = document.body;
let box = document.querySelector('.box');

// //Создание списка
function createNestedList(arr) {
   let li;
   let liContent = arr.map(elem => {
      if (Array.isArray(elem)) {
         li = `<li>${createNestedList(elem)}</li>`;
      } else {
         li = `<li> ${elem} </li>`;
      }
      return li;
   })

   let ul = `<ul>${liContent.join('')}</ul>`;
   return ul;
}

//Помещение списка в родителя
function createList(arr, parent = body) {
   parent.innerHTML = createNestedList(arr);
}

//Удаление по таймеру
function clearPage(sec) {
   let p = document.createElement('p');
   p.innerHTML = sec;
   body.prepend(p);

   //Удаление всего, кроме тега подключения скрипта
   let all = document.querySelectorAll('body > *:not(script)');

   let id = setInterval(function () {
      +p.innerHTML--;
      if (+p.innerHTML <= 0) {
         clearInterval(id);
         all.forEach(elem => elem.remove());
      }
   }, 1000)
}


let arrRes = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

createList(arrRes, box);
clearPage(3);






