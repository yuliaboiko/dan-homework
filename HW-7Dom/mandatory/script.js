'use strict'

function createList(arr, parent = body) {
   let ul = document.createElement('ul');
   let list = [];
   arr.map(elem => {
      let li = document.createElement('li');
      li.innerHTML = elem;
      list.push(li);
   })
   ul.append(...list);
   parent.prepend(ul);
}

let body = document.body;
let listArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let box = document.querySelector('.box');

createList(listArr, box);

