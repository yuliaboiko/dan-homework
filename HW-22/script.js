'use strict'
let body = document.body;
let table = document.createElement('table');

//Создание колонок и рядов в таблице
function createTable(col, row) {
   for (let i = 0; i < row; i++) {
      let tr = document.createElement('tr');
      for (let j = 0; j < col; j++) {
         let td = document.createElement('td');
         td.classList.add('on');
         tr.append(td);
      }
      table.append(tr);
   }
}
body.prepend(table);

createTable(30, 30);

//Окрас ячеек при клике
table.addEventListener('click', function (e) {
   if (e.target.tagName === 'TD') {
      e.target.classList.toggle('off');
   }
})

//Инвертирование ячеек при клике на боди
body.addEventListener('click', function (e) {
   if (e.target.tagName !== "TABLE" && e.target.tagName !== "TD" && e.target.tagName !== "TR") {
      table.classList.toggle('invert');
   }
})