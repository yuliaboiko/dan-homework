'use strict'

const password = document.querySelector('.password');
const repeatPassword = document.querySelector('.repeat-password');
const btn = document.querySelector('.btn');
// иконки глаз
const firstEye = document.querySelector('.first-eye');
const repeatEye = document.querySelector('.repeat-eye');
//Создание тега для ошибки
const errorMes = document.createElement('p');



firstEye.addEventListener('click', function () {
   changePassView(firstEye, password);
});

repeatEye.addEventListener('click', function () {
   changePassView(repeatEye, repeatPassword);
});

btn.addEventListener('click', function (e) {
   checkPassMatch();
   e.preventDefault();
});

// Отображение/скрытие пароля
function changePassView(eye, field) {
   if (eye.classList.contains('fa-eye')) {
      eye.classList.replace('fa-eye', 'fa-eye-slash');
      setType(field, 'text');
   } else {
      eye.classList.replace('fa-eye-slash', 'fa-eye');
      setType(field, 'password');
   }
}

//Изменение типа инпута
function setType(input, type) {
   input.type = type;
}

// проверка паролей на совпадение
function checkPassMatch() {
   if (password.value === repeatPassword.value) {
      errorMes.remove();
      setTimeout(() => alert('You are welcome'), 0);
      password.value = '';
      repeatPassword.value = '';
   } else {
      createErrorMes();
   }
}

// Формирование ошибки
function createErrorMes() {
   errorMes.innerHTML = 'Нужно ввести одинаковые значения';
   errorMes.style.color = 'red';
   btn.before(errorMes);
}

