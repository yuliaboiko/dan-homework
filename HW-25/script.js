'use strict'

const prevBtn = document.querySelector('.prev');
const nextBtn = document.querySelector('.next');
let sliderLine = document.querySelector('.slider-box');

let step = 0;

nextBtn.addEventListener('click', function () {
   step += 640;
   if (step > (5 * 640)) {
      step = 0;
   }
   sliderLine.style.transform = `translateX(${-step}px)`;
});

prevBtn.addEventListener('click', function () {
   step -= 640;
   if (step < 0) {
      step = 5 * 640;
   }
   sliderLine.style.transform = `translateX(${-step}px)`;
});