'use strict'

$(function () {
   $('.tabs').on('click', 'a', function () {

      $('.active').removeClass('active');
      $(this).parent().add($(this).attr('href')).addClass('active');

   })
})