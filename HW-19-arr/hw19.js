'use strict'

function checkDeadline(teamArr, tasksArr, deadlineDate) {

   // Макс. количество сторипоинтов, которые сделает команда за 1 день
   let teamMaxDayTasks = teamArr.reduce((sum, item) => sum + item);

   // Общая сумма сторипоинтов для решения всех задач
   let totalTasksNeed = tasksArr.reduce((sum, item) => sum + item);

   // Текущая дата
   let currentDay = new Date();

   // Промежуток между дедлайном и сегодня в днях без учета выходных
   let timeForWork = (deadlineDate - currentDay) / (1000 * 60 * 60 * 24);

   // Создаем массив заполненный цифрами, кол-во = промежуток между дедлайном и сегодня(дн.)
   let datesBetween = new Array(Math.round(timeForWork)).fill(0);
   let diffDaysArr = [];

   // Добавляем сегоднешнюю дату
   diffDaysArr.push(currentDay);
   /**
    * Записываем в массив даты между дедлайном и сегодня(плюсуем 1 день к предыдущему дню), за точку отчета - сегодня(мс)
    * 8.64e+7 - кол-во милисекунд в день
    */
   datesBetween.reduce((sum) => {
      sum = sum + 8.64e+7;
      diffDaysArr.push(new Date(sum));
      return sum;
   }, currentDay.getTime());

   // Получаем массив рабочих дней в промежутке
   let weekdays = diffDaysArr.filter(elem => elem.getDay() !== 0 && elem.getDay() !== 6);
   // Cколько часов нужно команде, чтобы решить задачи(без уч. вых-ых)
   let needHours = (totalTasksNeed / teamMaxDayTasks) * 8;

   // Cколько часов нужно с учетом вых-ых (-число -> нехватает часов, если +число -> лишние часы)
   let restDays = weekdays.length * 8 - needHours;

   /**
    * Если дедлайн попадает на выходной день -> то дедлайн переносится на ближайший будний, так как на выходных не работают
    * если до дедлайна остаются выходные дни, они не учитываются, как лишние дни -> на выходных работать не будут 
    */
   if (needHours / 8 < weekdays.length) {
      return (`Все задачи будут успешно выполнены за ${Math.round(weekdays.length - needHours / 8)} дней до наступления дедлайна!`)
   } else if (needHours / 8 === weekdays.length) {
      return (`Все задачи будут успешно выполнены точно в срок, дополнительных дней не осталось!`)
   } else {
      return (`Команде разработчиков придется потратить дополнительно ${Math.abs(restDays).toFixed(0)} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
   }
}

// Тут задаются значения аргументов для функции
let team = [5];
let tasks = [50];
let deadline = new Date(2021, 4, 12);

console.log(checkDeadline(team, tasks, deadline));

