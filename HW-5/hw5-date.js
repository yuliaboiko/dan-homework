'use strict'


function createNewUser() {

   const newUser = {
      getLogin() {
         return (this._firstName[0] + this._lastName).toLowerCase();
      },
      getAge() {
         let currentDate = new Date();
         // Формируем объект дата с переданными данными
         let userBirthday = new Date(
            this._birthday.split('.')[2],
            this._birthday.split('.')[1] - 1,
            this._birthday.split('.')[0]
         );
         //  Предварительно высчитываем возраст(без проверки на месяц)
         let age = currentDate.getFullYear() - userBirthday.getFullYear();

         //Проверка на месяц для корректировки возраста
         if (currentDate.getMonth() < userBirthday.getMonth() ||
            (currentDate.getMonth() === userBirthday.getMonth() &&
               currentDate.getDate() < userBirthday.getDate()
            )) {
            return --age;
         } else {
            return age;
         }
      },
      getPassword(){
         return this._firstName[0].toUpperCase() + this._lastName + this._birthday.split('.')[2];
      },

      // создает свойство _firstName и принимает значение value 
      set setFirstName(value) {
         this._firstName = value;
      },
      // создает свойство _lastName и принимает значение value 
      set setLastName(value) {
         this._lastName = value;
      },
      // создает свойство _birthday и принимает значение value 
      set setBirthday(value) {
         this._birthday = value;
      }

   }
   newUser.setFirstName = prompt('Введите Ваше имя');
   newUser.setLastName = prompt('Введите Вашу фамилию');
   newUser.setBirthday = prompt('Введите Ваш день рождения в формате дд.мм.гггг');

   return newUser;

}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


