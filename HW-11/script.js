'use strict'

let btnArr = document.querySelectorAll('button');

document.addEventListener('keydown', function (event) {
   setBtnColor(event.key);
});

function setBtnColor(text) {
   btnArr.forEach(btn => {
      btn.style.backgroundColor = '';
      if (btn.innerHTML.toUpperCase() === text.toUpperCase()) {
         btn.style.backgroundColor = 'blue';
      }
   })
}